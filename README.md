# Minecraft Server - 1.20.1 Forge

## Tartalomjegyzék
1. [Telepítés](#telepítés)
2. [Map](#map)
3. [Gamerules](#gamerules)
4. [Modok](#modok)


## Telepítés
- [I. Forge 1.20.1  letöltése és telepítése](https://adfoc.us/serve/sitelinks/?id=271228&url=https://maven.minecraftforge.net/net/minecraftforge/forge/1.20.1-47.2.20/forge-1.20.1-47.2.20-installer.jar)
- [II. Mods mappa letöltése innen](https://gitlab.com/snakecraft/minecraft-server-1.20.1-forge/-/archive/main/minecraft-server-1.20.1-forge-main.zip?path=mods)
- III. Modok behúzása C:\Users\"Felhasználónév"\AppData\Roaming\.minecraft\mods - mappába ha nincs ott csak hozd létre
- IV. Discordon pinelt IP-re csatlakozol

Ha valami probléma adódik javasolt a Java frissítése is.
Amennyiben modoltál már minecraftot forgeal és a mods mappában vannak már .jar fileok akkor hozz létre egy mappát a modson belül és húzd át abba a régieket. Ha visszaakarod őket majd rakni csak kicseréled a szerverhez kellőkkel amikor gondolod.

## Map
A map alap minecraft + create által generált, plusz építmények az Additional Structures és a MineColonies által.
(Kicsit részletesebb leírás ezekről a modok résznél)

A worldborder 40.000 blokkra van állítva tehát a max koordináták 
[20.000,20.000] - [-20.000,-20.000].
Ebből 5000 blokk sugárral Chunkyval előgenerálva lett.
Ebből következően ez a playzone a javasolt de tovább lehet menni egészen a borderig.

A map ebből a repoból letölthető, megpróbálom hetente frissíteni körülbelül.


## Gamerules
- doFireTick - false 
- disableElytraMovementCheck - true
- playersSleepingPercentage - 33

A tázterjedés griefelésen kívül megterhelő lehet a szerver számára ha akárhol elytrával repkedsz és természetes módon felgyullad valami, az ElytraCheck optimalizáció és desync esetén kickek elkerülésére van a SleepingPercentage pedig arra, hogy ne kelljen akárhányan mind elmenni aludni ha reggelt akar akárki.


## Modok
Gyakorlatilag 4 főbb mod van a listában nem túl elrugaszkodott.
- Create
- Chisel & Bits
- EmoteCraft
- Minecolonies

Ezeken kívül kisebb modok vannak, amik vagy a szerver/kliens optimalizálására szolgálnak vagy pedig előkövetelményei a felsoroltaknak.

### Create
Komplex mechanikus gépezetek, gyakorlatilag amit el tudsz képzelni meg tudsz vele valósítani; rendes lift, automata farm vízmalommal stb. .
Mivel nagyon sokszínű a mod javasolt tutorialok és a [wiki oldal nézegetése is](https://create.fandom.com/wiki/Create_Mod_Wiki).



### Minecolonies
Saját városok építése Minecrafton belül, komplex NPC-kel akik dolgoznak, építenek gyűjtögetnek. Ha tetszik a koncepció és építeni akarsz egy ilyesmi kolóniát javasolt a [wiki oldal meglátogatása](https://wiki.minecolonies.com/) vagy tutorial nézése online.

Almodjai sok új blokkot beraknak főleg dekorációként.


### Chisel & Bits
A blokkokat felosztja 16x16x16 "pixelre" ezeket vagy ki tudod vályni vagy miniblokkokat helyezgetni ahova szeretnél.

Más modos pakoló cuccokkal ezt ne keverjük ha lehet(Create Schematic / Minecolonies építések) mert necces hogy csak a kliensed hal bele vagy a szerver is :D

### EmoteCraft
Emoteolás minecraftban. Akármilyen emoteot le tudsz tölteni a netről .emotecraft / .json kiterjesztések és csak simán behúzod a C:\Users\"Felhasználónév"\AppData\Roaming\.minecraft\emotes mappába.
Ezután tudod is ezeket használni a játékban ha beállítod őket nem szükséges a szerverre feltölteni elég ha nálad vannak a fileok.
