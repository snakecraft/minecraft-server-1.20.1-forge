tellraw @a {"text": "*-----------------------------------------------*","color": "#00FFFF"}
tellraw @a {"text": "SnakePack - Creating scoreboard objectives snakepack_deathcounter & snakepack_max_deaths","color": "#00FFFF"}
scoreboard objectives add snakepack_deathcounter deathCount "Deaths - SP"
scoreboard objectives add snakepack_max_deaths dummy "Max Death Count- SP"

tellraw @a {"text": "SnakePack - Setting the objective snakepack_max_deaths to player \"$snakepack_max_deaths_var\" to 3","color": "#00FFFF"}
scoreboard players set $snakepack_max_deaths_var snakepack_max_deaths 3
tellraw @a {"text": "If you wish to setup a different max death count change \"$snakepack_max_deaths_var\"'s snakepack_max_deaths objective score to the given number.","color": "#076eb2","italic": true}
tellraw @a {"text": "For example : /scoreboard players set $snakepack_max_deaths_var snakepack_max_deaths 5 ","color": "#076eb2","italic": true}
tellraw @a {"text": "*-----------------------------------------------*","color": "#00FFFF"}
